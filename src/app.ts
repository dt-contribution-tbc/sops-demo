import express, { Express, Request, Response } from "express";
import cors from "cors";
import pino from "pino";
import dotenv from "dotenv";
import { HelloRoutes } from "./modules/hello/hello.routes";

dotenv.config();

export const Logger = pino({
  // Default Level: Info
  level: process.env.LOG_LEVEL || "info",
  transport: {
    target: "pino-pretty",
    options: {
      colorize: true,
      levelFirst: true,
      translateTime: "yyyy-mm-dd, h:MM:ss TT",
    },
  },
});

const app: Express = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const routes = [new HelloRoutes()];

Logger.debug(`${routes.length} Routes loaded`);

routes.forEach((item) => app.use("/", item.router));

// for testing
export default app;
