import app, { Logger } from "./app";

const port = process.env.PORT || 3000;

app.listen(port, () => {
  Logger.info(`server running at localhost:${port}`);
});
