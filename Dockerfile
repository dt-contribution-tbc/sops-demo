FROM node:22-alpine AS base
FROM base AS deps

USER 0

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY package.json pnpm-lock.yaml* ./
RUN yarn global add pnpm && pnpm i --frozen-lockfile

FROM base AS builder
WORKDIR /app

USER 0

COPY --from=deps /app/node_modules ./node_modules
COPY . .
RUN npm run build

FROM base AS runner
WORKDIR /app

USER 0

ENV NODE_ENV production

COPY --from=builder --chown=node:node /app/dist ./
COPY package.json pnpm-lock.yaml* ./
RUN yarn global add pnpm && pnpm i --production --frozen-lockfile

USER node:node

EXPOSE 3000
ENV PORT 3000

CMD ["node", "server.js"]
