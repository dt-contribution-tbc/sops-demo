# HelloWorld

HelloWorld API written in expressJS with TypeScript

## Environment variables

The following environment variables are used in this project:
| ENV | Description | Default |
|--------------------------|----------------------------------------|--------------------------------|
| `PORT` | Port of the api | `3000` |
| `LOG_LEVEL` | Log-Level of pino logger | `info` |

## Installation

1. set up `pnpm` on your local machine
1. run `pnpm install`

## Running the app

First rename `example.env` to `.env` and adjust the environment variables.

Run as node application:

```sh
pnpm build
pnpm start
```

or as docker container:

```sh
docker build . -t helloworld
run -it -p 3000:3000 helloworld
```

## Contributing

tbd.

## API Testing

### API Client

1. Download / install [bruno](https://www.usebruno.com/downloads)
1. Open collection by clicking the "3 dots menu" -> "Open Collection" -> Navigate to `requests`-folder
1. Set Environment to `local`

### Test Framework

The testing framework jest has been configured in this project. Simply run:

```sh
pnpm test
```
